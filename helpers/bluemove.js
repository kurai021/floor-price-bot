//const dotenv = require('dotenv');
//dotenv.config();

const puppeteerPath = require('puppeteer');
const puppeteer = require('puppeteer-extra');
const StealthPlugin = require('puppeteer-extra-plugin-stealth');
puppeteer.use(StealthPlugin());
const randomUseragent = require('random-useragent');

async function getFloorPrice(slug, name, interaction, EmbedBuilder) {
    const USER_AGENT = 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.75 Safari/537.36';

    const browser = await puppeteer.launch(
        {
            headless: true, executablePath: puppeteerPath.executablePath() || null, args: [
                '--no-sandbox', '--disable-setuid-sandbox'
            ], ignoreHTTPSErrors: true, dumpio: false
        }
    );

    try {
        const page = await browser.newPage()
        const userAgent = randomUseragent.getRandom();
        const UA = userAgent || USER_AGENT;

        //Randomize viewport size
        await page.setViewport({
            width: 1920 + Math.floor(Math.random() * 100),
            height: 3000 + Math.floor(Math.random() * 100),
            deviceScaleFactor: 1,
            hasTouch: false,
            isLandscape: false,
            isMobile: false,
        });

        await page.setUserAgent(UA);
        await page.setJavaScriptEnabled(true);
        await page.setDefaultNavigationTimeout(0);

        await page.goto(`https://aptos-mainnet-api.bluemove.net/api/market-items?filters[collection][slug][$eq]=${slug}&filters[status][$eq]=1&filters[price][$gte]=0&filters[price][$lte]=10000000000000000&sort[0]=price%3Aasc&pagination[page]=1&pagination[pageSize]=24`,{waitUntil: 'networkidle0'})
        const resCollection = await page.waitForSelector("body > pre")
        const textCollection = await page.evaluate(web => web.textContent, resCollection)

        const nftCollection = JSON.parse(textCollection);
        const price = (nftCollection.data[0].attributes.price/100000000)
        const nftFloor = nftCollection.data[0].attributes.uri

        await page.goto(nftFloor)
        const resFloor = await page.waitForSelector("body > pre")
        const textFloor = await page.evaluate(web => web.textContent, resFloor)

        const nftItem = JSON.parse(textFloor)
        const ipfsToHTTP = nftItem.image.replace("ipfs://", "https://ipfs.io/ipfs/")

        const embed = new EmbedBuilder()
            .setColor(0x0099ff)
            .setTitle(`${name} Floor Price Tracker`)
            .setDescription(`${nftItem.description}`)
            .setImage(`${ipfsToHTTP}`)
            .addFields({name: "Name", value: `${nftItem.name}`})
            .addFields({name: "Price", value: `${price} APT`})

        nftItem.attributes.forEach(attribute => {
            embed.addFields({name: attribute.trait_type, value: attribute.value, inline: true})
        })

        await interaction.channel.send({embeds: [embed]})
    } catch (error) {
        // Your chance to handle errors
        console.log(error)
        await interaction.channel.send("There was an error during the search process")
    } finally {
        // Always close the browser
        await browser.close();
    }
}

module.exports = {
    getFloorPrice: getFloorPrice
}