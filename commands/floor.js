const { SlashCommandBuilder, EmbedBuilder } = require('discord.js');
const bluemove = require("../helpers/bluemove")

module.exports = {
	data: new SlashCommandBuilder()
		.setName('floor')
		.setDescription('Replies with NFT floor'),
	async execute(interaction) {
		await interaction.deferReply();
        await bluemove.getFloorPrice(process.env.SLUG, process.env.COLLECTION_NAME, interaction, EmbedBuilder)
		await interaction.deleteReply();
	},
};