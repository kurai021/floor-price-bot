## How to configure

Before starting I assume you have basic knowledge of Node.js and Ubuntu Server

- Set these enviromment vars (`BOT_TOKEN`, `CLIENTID`, `SLUG`, `COLLECTION_NAME`, `CHANNEL`):

`BOT_TOKEN` is your Discord Developer Portal bot token (see: [https://discord.com/developers/docs/getting-started](https://discord.com/developers/docs/getting-started) for more details)

`CLIENTID` is your Client ID (registering your bot you will receive a client ID and a client secret)

`SLUG` is the path after [https://bluemove.net/collection](https://bluemove.net/collection) in your collection

`COLLECTION_NAME` is your collection name with spaces and Uppercases

`CHANNEL` is the channel where the bot will be available for check floor

- This bot depends on Puppeteer, you should install all the system dependencies: In Ubuntu 20.04 or 22.04 install them with `sudo apt-get install gconf-service libasound2 libatk1.0-0 libc6 libcairo2 libcups2 libdbus-1-3 libexpat1 libfontconfig1 libgcc1 libgconf-2-4 libgdk-pixbuf2.0-0 libglib2.0-0 libgtk-3-0 libnspr4 libpango-1.0-0 libpangocairo-1.0-0 libstdc++6 libx11-6 libx11-xcb1 libxcb1 libxcomposite1 libxcursor1 libxdamage1 libxext6 libxfixes3 libxi6 libxrandr2 libxrender1 libxss1 libxtst6 ca-certificates fonts-liberation libappindicator1 libnss3 lsb-release xdg-utils wget libatk-bridge2.0-0 libgbm-dev`

- Before run your bot, you should deploy the command as is showed in this guide: [https://discordjs.guide/creating-your-bot/command-deployment.html#guild-commands](https://discordjs.guide/creating-your-bot/command-deployment.html#guild-commands), this is easy with `npm run deploy`.

- Run the server with `npm start`